import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {

  apiUrl: string = 'https://swapi.co/api/'
  constructor(private http: HttpClient) { }

  //declare methods of the service

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return throwError('Something bad happened, please try again later')
    };
  }
  getParamData(number, category) {
    return this.http.get(`${this.apiUrl}${category}/${number}/`)
      .pipe(
        catchError(this.handleError<Object[]>('getParamData', []))
      );
  }
}
