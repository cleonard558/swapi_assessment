import { Component, OnInit } from '@angular/core';
import { PlaceholderService } from './placeholder.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  constructor(private placeholderService:PlaceholderService) { }

  //   //declare models for this component

  // //users // Object[] = []
  formId:number =2
  whichCategory:string ='people'
  choices:Object[] = [{cat:'people'},{cat:'planets'},{cat:'species'},{cat:'vehicles'},{cat:'starships'}]
  
  model //we could do with a class here to remove the browser errors
  handleClick(){
    // invoke the service passing paramaters
    this.placeholderService.getParamData(this.formId,this.whichCategory).subscribe((result)=>{this.model= result})
  }

  ngOnInit() {
  }

}
