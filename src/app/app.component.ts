import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//import { PlaceholderService } from './placeholder.service';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  //declare models for this component

  //users // Object[] = []
  formId:number =2
  whichCategory:string ='people'
  choices:Object[] = [{cat:'people'},{cat:'planets'},{cat:'species'},{cat:'vehicles'},{cat:'starships'}]
  
  model //we could do with a class here to remove the browser errors

  //declare functions
  //constructor(private placeholderService:PlaceholderService) {

  //}

  // handleClick(){
  //   // invoke the service passing paramaters
  //   this.placeholderService.getParamData(this.formId,this.whichCategory).subscribe((result)=>{this.model= result})
  // }
}